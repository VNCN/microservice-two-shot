import React from 'react';

class HatForm extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            styleName: '',
            hatColor: '',
            hatUrl: '',
            location: '',
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.style_name = data.styleName
        data.hat_color = data.hatColor
        data.hat_url = data.hatUrl
        delete data.styleName
        delete data.hatColor
        delete data.hatUrl
        delete data.locations
        console.log(data);

        const HatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                },
        };
        
        const response = await fetch(HatsUrl, fetchConfig);
        
        if (response.ok) {
            const newHat = await response.json();
            
        
            const cleared = {
              fabric: '',
              styleName: '',
              hatColor: '',
              hatUrl: '',
              location: '',
            };

            this.setState(cleared);
          }
        }

    handleFabricChange(event) {
      const value = event.target.value;
      this.setState({fabric: value})
      }

    handleStyleChange(event) {
      const value = event.target.value;
      this.setState({styleName: value})
      }

    handleColorChange(event) {
      const value = event.target.value;
      this.setState({hatColor: value})
      }

    handlePictureChange(event) {
      const value = event.target.value;
      this.setState({hatUrl: value})
      }

    handleLocationChange(event) {
      const value = event.target.value;
      this.setState({location: value})
      }

    

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            }
        }

  render() {
    return (
      <React.Fragment>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Hat</h1>
                    <form onSubmit={this.handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                    <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required
                        type="text" name="fabric" id="fabric"
                        className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleStyleChange} value={this.state.styleName} placeholder="Style Name" required type="text" name="styleName" id="styleName" className="form-control"/>
                        <label htmlFor="styleName">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleColorChange} value={this.state.hatColor} placeholder="Color" required type="text" name="hatColor" id="hatColor" className="form-control"/>
                        <label htmlFor="hatColor">Color</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="hatUrl" className ="form-label">Picture Url</label>
                        <input onChange={this.handlePictureChange} value={this.state.hatUrl}  name="hatUrl" required type="url" id="hatUrl" className="form-control" rows="3"></input>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {this.state.locations.map(location => {
                          return (
                            <option key={location.id} value={location.href}>{location.closet_name}</option>
                          );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
      </React.Fragment>
    );
  }
}

export default HatForm;
