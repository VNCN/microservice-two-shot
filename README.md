# Wardrobify

Team:

* Randy Angel - Hats
* Vincent Lee - Shoes

## Design

## Shoes microservice

Created a Shoe and Bin model
Installed the shoes app into INSTALLED_APPS in settings
Created a view function that is able to send "GET" and "POST" requests
Configured URLs
Sent a request to server through Insomnia to check data is pulling
Created a poller to poll data and made a POST request to create bin data
BinVO was created to poll data from shoes api
Created a function to "DELETE" entries
Updated the App.js file to update the navigation bar
Created a function in index.js to pull data from apis
Created a ShoesList.js and added stylings to the table that lists shoe data
Create a ShoesForm.js to handle submit and all property changes
Updated ShoesForm.js with JSX to populate the browser page

## Hats microservice
Hats API: the RESTful API to interact with Hat resources
Hats Poller: the poller to poll the Wardrobe API for Location resources
Wardrobe API: provides Location RESTful API endpoints
Database: the PostgreSQL database that will hold the data of all of the microservices
React: the React-based front-end application displaying the components to interact with our services
